const queryDict = {};
window.location.search.substr(1).split('&').forEach((item) => {
  queryDict[item.split('=')[0]] = item.split('=')[1];
});

if (queryDict.text !== undefined) {
  document.getElementById('glyphus-input').value = decodeURIComponent(queryDict.text);
}

function updateClipboard(text) {
  navigator.clipboard.writeText(text).then(() => {
  }, () => {
    console.warn('Can not use clipboard api.');
  });
}

document.getElementById('share-button').addEventListener('click', () => {
  window.history.pushState('', '', `?text=${encodeURIComponent(document.getElementById('glyphus-input').value)}`);
  document.getElementById('button-feedback-text').innerHTML = 'Link copied!';
  setTimeout(() => { document.getElementById('button-feedback-text').innerHTML = ''; }, 1000);
  updateClipboard(window.location.href);
});

document.getElementById('extend-sidebar-button').addEventListener('click', () => {
  document.getElementById('main-content').classList.add('two-thirds');
  document.getElementById('sidebar').style.display = 'block';
  document.getElementById('button-feedback-text').innerHTML = 'Move your mouse over the glyphs!';
  setTimeout(() => { document.getElementById('button-feedback-text').innerHTML = ''; }, 2000);
});
