import alphabet from './alphabet.js';

class GraphFactory {
  constructor() {
    this.alphabet = alphabet;
    this.nbBitsLetter = Math.max(...Object.values(this.alphabet).map(letterCode => letterCode.toString(2).length));
  }

  lettersToPointsAndSegments(glyphLetters, drawGraph = false) {
    const pointsDict = this.lettersToPointsDict(glyphLetters);
    const graph = Object.values(GraphFactory.verticesToGraph(pointsDict));
    let selectedGraph = graph;

    if (!drawGraph) {
      const msts = GraphFactory.getMsts(graph);
      // const msts = GraphFactory.yamada(graph);

      const nbEdgesPerPointPerMst = msts.map(mst => GraphFactory.getNbEdgesPerVertex(mst));
      const nbIntersectionsPerMst = nbEdgesPerPointPerMst.map(nbEdgesPerPoint => Object.values(nbEdgesPerPoint)
        .reduce((a, b) => a + (b > 2 ? 1 : 0), 0));
      const minNbIntersections = Math.min(...nbIntersectionsPerMst);
      const goodMsts = msts.filter((mst, id) => nbIntersectionsPerMst[id] === minNbIntersections);
      selectedGraph = goodMsts[0];
    }

    const nbEdgesPerVertex = GraphFactory.getNbEdgesPerVertex(selectedGraph);
    for (const [pointName, point] of Object.entries(pointsDict)) {
      point.nbEdges = nbEdgesPerVertex[pointName] === undefined ? 0 : nbEdgesPerVertex[pointName];
    }

    const segments = selectedGraph.map(edge => ({
      from: GraphFactory.vertexToPoint(edge.from),
      to: GraphFactory.vertexToPoint(edge.to),
    }));

    return [Object.values(pointsDict), segments];
  }

  static getNbEdgesPerVertex(graph) {
    const nbEdgesPerVertex = {};
    for (const edge of graph) {
      nbEdgesPerVertex[edge.from] = edge.from in nbEdgesPerVertex ? nbEdgesPerVertex[edge.from] + 1 : 1;
      nbEdgesPerVertex[edge.to] = edge.to in nbEdgesPerVertex ? nbEdgesPerVertex[edge.to] + 1 : 1;
    }
    return nbEdgesPerVertex;
  }

  static vertexToPoint(vertex) {
    const [x, y] = vertex.split(',').map(n => parseInt(n, 10));
    return { x, y };
  }

  static getGraphWeight(graph) {
    return graph.reduce((total, edge) => total + edge.weight, 0);
  }

  static getMsts(graph) {
    const vertices = GraphFactory.getVertices(graph);
    const sortedEdges = graph.sort((a, b) => a.weight - b.weight);

    const msts = [];
    msts[0] = GraphFactory.kruskal(vertices, sortedEdges);
    const mstWeight = GraphFactory.getGraphWeight(msts[0]);
    for (const mstEdge of msts[0]) {
      if (graph.filter(edge => edge.weight === mstEdge.weight).length > 1) {
        const newGraph = graph.map(edge => (edge.from === mstEdge.from && edge.to === mstEdge.to
          ? [edge.from, edge.to, edge.weight + 1] : edge));
        const newMst = GraphFactory.kruskal(vertices, newGraph);
        if (mstWeight === GraphFactory.getGraphWeight(newMst)) {
          msts.push(newMst);
        }
      }
    }
    return msts;
  }

  static getVertices(graph) {
    const vertices = [];
    for (const edge of graph) {
      if (!vertices.includes(edge.from)) {
        vertices.push(edge.from);
      }
      if (!vertices.includes(edge.to)) {
        vertices.push(edge.to);
      }
    }
    return vertices;
  }

  // static yamada(graph) {
  //   const vertices = GraphFactory.getVertices(graph);
  //   const sortedEdges = graph.sort((a, b) => a.weight - b.weight);
  //
  //   return [GraphFactory.kruskal(vertices, sortedEdges)];
  // }

  // static kruskal(vertices, sortedEdges, requiredEdges = [], forbiddenEdges = []) {
  static kruskal(vertices, sortedEdges) {
    const mst = [];
    const forest = {};

    vertices.forEach((vertex, key) => {
      forest[vertex] = key;
    });

    for (const edge of sortedEdges) {
      // if (forest[edge.from] !== forest[edge.to] && !(edge in forbiddenEdges)) {
      if (forest[edge.from] !== forest[edge.to]) {
        const aux = forest[edge.to];
        forest[edge.to] = forest[edge.from];
        for (const vertex in forest) {
          if (forest[vertex] === aux) {
            forest[vertex] = forest[edge.from];
          }
        }
        mst.push(edge);
      }
    }
    return mst;
  }

  static verticesToGraph(vertices) {
    const graphDict = {};

    // edge length x1000 to avoid floats calculations
    for (const [from, p] of Object.entries(vertices)) {
      // Adjacent points: distance = 1 -> edge length = 1000
      const adjacent = [
        `${p.x + 1},${p.y}`, `${p.x - 1},${p.y}`, // horizontals
        `${p.x},${p.y + 1}`, `${p.x},${p.y - 1}`]; // verticals
      for (const to of adjacent) {
        const edgeId = [from, to].sort().join(';');
        if (to in vertices) {
          if (!(edgeId in graphDict)) {
            graphDict[edgeId] = { from, to, weight: 1000 };
          }
        }
      }

      // Diagonal points: distance = sqrt(1²+1²) = sqrt(2) =~ 1.414 -> edge length = 1414
      const diagonals = [
        `${p.x + 1},${p.y + 1}`, `${p.x + 1},${p.y - 1}`, // rights
        `${p.x - 1},${p.y + 1}`, `${p.x - 1},${p.y - 1}`]; // lefts
      for (const to of diagonals) {
        const edgeId = [from, to].sort().join(';');
        if (to in vertices) {
          if (!(edgeId in graphDict)) {
            graphDict[edgeId] = { from, to, weight: 1414 };
          }
        }
      }

      // Knight points: distance = sqrt(1²+2²) = sqrt(5) =~ 2.236 -> edge length = 2236
      const knights = [
        `${p.x + 2},${p.y + 1}`, `${p.x + 2},${p.y - 1}`, // rights
        `${p.x - 2},${p.y + 1}`, `${p.x - 2},${p.y - 1}`, // lefts
        `${p.x + 1},${p.y + 2}`, `${p.x - 1},${p.y + 2}`, // bottoms
        `${p.x + 1},${p.y - 2}`, `${p.x - 1},${p.y - 2}`]; // tops
      for (const to of knights) {
        const edgeId = [from, to].sort().join(';');
        if (to in vertices) {
          if (!(edgeId in graphDict)) {
            graphDict[edgeId] = { from, to, weight: 2236 };
          }
        }
      }
    }

    return Object.values(graphDict);
  }

  letterToCode(letter) {
    return this.alphabet[letter];
  }

  lettersToPointsDict(letters) {
    const points = {};
    for (let i = 0; i < letters.length; i += 1) {
      for (let j = 0; j < this.nbBitsLetter; j += 1) {
        if (((this.letterToCode(letters[i]) >> j) & 1) === 1) {
          points[`${i},${j}`] = { x: i, y: j };
        }
      }
    }
    return points;
  }
}

export default GraphFactory;
