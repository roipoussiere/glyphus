const dotTypes = ['alone', 'extremity', 'inline', 'intersection'];

class Glyph {
  constructor(text, config) {
    this.svg = document.createElement('svg');
    this.svg.classList.add('glyph');

    if (config.showTooltip) {
      const title = document.createElement('title');
      title.appendChild(document.createTextNode(text));
      this.svg.appendChild(title);
    }

    // config.padding unit is the space between 2 nearby dots.
    const viewBox = {
      x: -config.padding.left,
      y: -config.padding.top,
      width: text.length - 1 + config.padding.left + config.padding.right,
      height: config.nbBitsLetter - 1 + config.padding.top + config.padding.bottom,
    };
    this.svg.setAttribute('viewBox', `${viewBox.x} ${viewBox.y} ${viewBox.width} ${viewBox.height}`);
  }

  drawSegments(edges, className = '') {
    let container = this.svg;
    if (className !== '') {
      container = document.createElement('g');
      container.classList.add(className);
      this.svg.appendChild(container);
    }
    for (const edge of edges) {
      container.appendChild(Glyph.makeEdge(edge.from, edge.to));
    }
  }

  drawDots(points) {
    for (const point of points) {
      const dot = Glyph.makeDot(point);
      if (dot !== undefined) {
        this.svg.appendChild(dot);
      }
    }
  }

  static makeEdge(from, to) {
    const path = document.createElement('path');
    path.setAttribute('d', `M${from.x},${from.y} L${to.x},${to.y}`);
    return path;
  }

  static makeDot(point) {
    const path = document.createElement('path');

    path.classList.add('glyph-dot');
    if ('nbEdges' in point) {
      path.classList.add(`glyph-dot-${point.nbEdges < 3 ? dotTypes[point.nbEdges] : dotTypes[3]}`);
    }
    path.setAttribute('transform', `rotate(45, ${point.x}, ${point.y})`);
    path.setAttribute('d', `M${point.x},${point.y} h0`);

    return path;
  }
}

export default Glyph;
