import Glyph from './glyph.js';
import GraphFactory from './graphFactory.js';

class Sidebar {
  constructor() {
    this.graphFactory = new GraphFactory();
    this.viewboxUnitSize = this.getViewboxUnitSize();
  }

  getViewboxUnitSize() {
    const tile = document.createElement('svg');
    tile.id = 'sidebar-plane';
    document.getElementById('sandbox').appendChild(tile);
    return parseFloat(getComputedStyle(tile).height) / (this.graphFactory.nbBitsLetter - 1);
  }

  update(target) {
    this.target = target.nodeName === 'path' ? target.parentNode : target;
    this.fillLetters();
    this.fillGraph();
    this.fillMsts();
    this.fillgoodMst();
  }

  getFigureHeight(config) {
    return (this.graphFactory.nbBitsLetter - 1 + config.padding.top + config.padding.bottom) * this.viewboxUnitSize;
  }

  fillLetters() {
    const config = {
      padding: {
        left: 1,
        right: 0.6,
        top: 3.1,
        bottom: 1.1,
      },
      showTooltip: false,
      nbBitsLetter: this.graphFactory.nbBitsLetter,
      overCallback: () => {},
    };
    const sidebarLetters = document.getElementById('sidebar-letters-container');
    while (sidebarLetters.firstChild) {
      sidebarLetters.removeChild(sidebarLetters.firstChild);
    }

    const lettersTable = new Glyph(this.target.textContent, config);
    lettersTable.svg.classList.add('sidebar-figure');

    // draw table
    const table = document.createElement('path');
    const leftLimit = -0.5;
    const rightLimit = this.target.textContent.length - 1 + 0.5;
    const topLimit = -3.0;
    const bottomLimit = this.graphFactory.nbBitsLetter - 1 + 1.0;

    let d = '';
    for (let x = 0; x < this.target.textContent.length + 1; x += 1) {
      d += `M${x - 0.5},${topLimit} L${x - 0.5},${bottomLimit}`;
    }
    d += `M${leftLimit},${topLimit} L${rightLimit},${topLimit}`;
    d += `M${leftLimit},-1.0 L${rightLimit},-1.0`;
    d += `M${leftLimit},${bottomLimit} L${rightLimit},${bottomLimit}`;
    table.setAttribute('d', d);
    table.classList.add('table');
    lettersTable.svg.appendChild(table);

    // draw grid
    for (let x = 0; x < this.target.textContent.length; x += 1) {
      const letterGrid = document.createElement('path');
      d = `M${x},0 L${x},${this.graphFactory.nbBitsLetter - 1}`;
      for (let y = 0; y < this.graphFactory.nbBitsLetter; y += 1) {
        d += `M${x - 0.2},${y} L${x + 0.2},${y}`;
      }
      letterGrid.setAttribute('d', d);
      letterGrid.classList.add('grid');
      lettersTable.svg.appendChild(letterGrid);
    }

    // draw labels
    for (let x = 0; x < this.target.textContent.length; x += 1) {
      const text = document.createElement('text');
      text.setAttribute('x', `${x}`);
      text.setAttribute('y', '-1.5');
      text.classList.add('label-letters');
      text.innerHTML = this.target.textContent[x] || '.';
      lettersTable.svg.appendChild(text);
    }

    const points = Object.values(this.graphFactory.lettersToPointsDict(this.target.textContent));
    lettersTable.drawDots(points);

    lettersTable.svg.style.height = `${this.getFigureHeight(config)}px`;
    sidebarLetters.appendChild(lettersTable.svg);
    sidebarLetters.innerHTML += '';
  }

  fillGraph() {
    const config = {
      padding: {
        left: 1,
        right: 0.6,
        top: 1.4,
        bottom: 0.3,
      },
      showTooltip: false,
      nbBitsLetter: this.graphFactory.nbBitsLetter,
      overCallback: () => {},
    };

    const sidebarGraphContainer = document.getElementById('sidebar-graph-container');
    while (sidebarGraphContainer.firstChild) {
      sidebarGraphContainer.removeChild(sidebarGraphContainer.firstChild);
    }

    const sidebarGraph = new Glyph(this.target.textContent, config);
    sidebarGraph.svg.classList.add('sidebar-figure');

    // draw grid
    const grid = document.createElement('path');
    let d = '';
    for (let x = 0; x < this.target.textContent.length; x += 1) {
      d += `M${x},0 L${x},${this.graphFactory.nbBitsLetter - 1}`;
    }
    for (let y = 0; y < this.graphFactory.nbBitsLetter; y += 1) {
      d += `M0,${y} L${this.target.textContent.length - 1},${y}`;
    }
    grid.setAttribute('d', d);
    grid.classList.add('grid');
    sidebarGraph.svg.appendChild(grid);

    // draw labels
    const xLabels = 'abcdefghijklmnopqrstuvwxyz';
    for (let x = 0; x < this.target.textContent.length; x += 1) {
      const text = document.createElement('text');
      text.setAttribute('x', `${x}`);
      text.setAttribute('y', '-0.5');
      text.innerHTML = xLabels[x] || '.';
      sidebarGraph.svg.appendChild(text);
    }
    for (let y = 0; y < this.graphFactory.nbBitsLetter; y += 1) {
      const text = document.createElement('text');
      text.setAttribute('x', '-0.6');
      text.setAttribute('y', `${y + 0.3}`);
      text.innerHTML = y.toString();
      sidebarGraph.svg.appendChild(text);
    }

    const [points, edges] = this.graphFactory.lettersToPointsAndSegments(this.target.textContent, true);
    sidebarGraph.drawSegments(edges);
    sidebarGraph.drawDots(points);

    sidebarGraph.svg.style.height = `${this.getFigureHeight(config)}px`;
    sidebarGraphContainer.appendChild(sidebarGraph.svg);
    sidebarGraphContainer.innerHTML += '';
  }

  fillMsts() {
    const config = {
      padding: {
        left: 0.1,
        right: 0.1,
        top: 0.1,
        bottom: 0.1,
      },
      showTooltip: false,
      nbBitsLetter: this.graphFactory.nbBitsLetter,
      overCallback: () => {},
    };

    const sidebarMstsContainer = document.getElementById('sidebar-msts-container');
    while (sidebarMstsContainer.firstChild) {
      sidebarMstsContainer.removeChild(sidebarMstsContainer.firstChild);
    }

    const pointsDict = this.graphFactory.lettersToPointsDict(this.target.textContent);
    const [, edges] = this.graphFactory.lettersToPointsAndSegments(this.target.textContent, true);
    const graph = Object.values(GraphFactory.verticesToGraph(pointsDict));
    for (const mst of GraphFactory.getMsts(graph)) {
      const sidebarMst = new Glyph(this.target.textContent, config);
      sidebarMst.svg.classList.add('sidebar-figure');
      sidebarMst.svg.classList.add('sidebar-mst');

      const segments = mst.map(edge => ({
        from: GraphFactory.vertexToPoint(edge.from),
        to: GraphFactory.vertexToPoint(edge.to),
      }));
      sidebarMst.drawSegments(edges, 'background-segments');
      sidebarMst.drawSegments(segments);

      sidebarMst.svg.style.height = `${this.getFigureHeight(config)}px`;
      sidebarMstsContainer.appendChild(sidebarMst.svg);
      sidebarMstsContainer.innerHTML += '';
    }
  }

  fillgoodMst() {
    const config = {
      padding: {
        left: 0.1,
        right: 0.1,
        top: 0.1,
        bottom: 0.1,
      },
      showTooltip: false,
      nbBitsLetter: this.graphFactory.nbBitsLetter,
      overCallback: () => {},
    };

    const sidebarMstsContainer = document.getElementById('sidebar-good-mst-container');
    while (sidebarMstsContainer.firstChild) {
      sidebarMstsContainer.removeChild(sidebarMstsContainer.firstChild);
    }

    const [, graph] = this.graphFactory.lettersToPointsAndSegments(this.target.textContent, true);
    const [, mst] = this.graphFactory.lettersToPointsAndSegments(this.target.textContent);

    const sidebarMst = new Glyph(this.target.textContent, config);
    sidebarMst.svg.classList.add('sidebar-figure');
    sidebarMst.svg.classList.add('sidebar-mst');

    sidebarMst.drawSegments(graph, 'background-segments');
    sidebarMst.drawSegments(mst);

    sidebarMst.svg.style.height = `${this.getFigureHeight(config)}px`;
    sidebarMstsContainer.appendChild(sidebarMst.svg);
    sidebarMstsContainer.innerHTML += '';
  }
}

export default Sidebar;
