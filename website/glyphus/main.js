import DrawingArea from './drawingArea.js';

const glyphusContainers = {};

document.addEventListener('DOMContentLoaded', () => {
  for (const glyphusContainer of document.getElementsByClassName('glyphus-area')) {
    const glyphusId = glyphusContainer.dataset.input;
    const source = document.getElementById(glyphusId);

    if (source === null) {
      console.warn(`Glyphus: Element with id ${glyphusContainer.dataset.input} was not found in the page.`);
      break;
    }

    glyphusContainer.style['overflow-y'] = 'auto';

    const drawingArea = new DrawingArea(glyphusContainer);
    glyphusContainers[glyphusId] = drawingArea;

    source.addEventListener('input', () => drawingArea.draw(source.value));
    drawingArea.draw(source.value);
  }
}, false);
