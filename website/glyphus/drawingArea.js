import GraphFactory from './graphFactory.js';
import Glyph from './glyph.js';
import Sidebar from './sidebar.js';

class DrawingArea {
  constructor(container) {
    this.graphFactory = new GraphFactory();

    this.container = container;
    this.text = '';

    this.sidebar = new Sidebar();

    this.config = {
      padding: {
        left: 0.5, right: 0.5, top: 0.5, bottom: 0.5,
      },
      showTooltip: true,
      nbBitsLetter: this.graphFactory.nbBitsLetter,
      overCallback: event => this.sidebar.update(event.target),
    };
  }

  draw(text) {
    this.text = text;
    this.container.setAttribute('aria-label', text);
    this.container.setAttribute('role', 'figure');
    this.clear();
    this.appendText(this.text);
    for (const svg of this.container.getElementsByClassName('glyph')) {
      svg.addEventListener('mouseover', event => this.config.overCallback(event));
    }
  }

  clear() {
    while (this.container.firstChild) {
      this.container.removeChild(this.container.firstChild);
    }
  }

  appendText(text) {
    const supportedLetters = Object.keys(this.graphFactory.alphabet).join('');

    let lastGlyph = false;
    let isLineBreak = false;
    for (const line of text.split(/([\n\r]+)/g)) {
      if (isLineBreak) {
        this.insertLineBreak();
      } else {
        let isGlyph = false;
        for (const lettersGroup of line.split(new RegExp(`([${supportedLetters}]+)`, 'g'))) {
          if (isGlyph) {
            lastGlyph = this.insertSvg(lettersGroup);
          } else {
            this.insertText(lettersGroup);
          }
          isGlyph = !isGlyph;
        }
      }
      isLineBreak = !isLineBreak;
    }
    if (lastGlyph) {
      this.sidebar.update(lastGlyph);
    }
  }

  insertText(text) {
    const span = document.createElement('span');
    span.classList.add('callback-text');
    span.innerHTML = text;
    this.container.appendChild(span);
  }

  insertSvg(lettersGroup) {
    const [points, edges] = this.graphFactory.lettersToPointsAndSegments(lettersGroup);
    const glyph = new Glyph(lettersGroup, this.config);
    glyph.drawSegments(edges);
    glyph.drawDots(points);
    this.container.appendChild(glyph.svg);
    this.container.innerHTML += ''; // thanks SO https://stackoverflow.com/questions/21347833
    return glyph.svg;
  }

  insertLineBreak() {
    this.container.appendChild(document.createElement('br'));
  }
}

export default DrawingArea;
