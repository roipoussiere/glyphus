// The alphabet is ordered to minimize the total number of dots in the text:
// We associate most used letters to bits combination that have the less number of '1', following a process similar to
// the Huffman coding (https://en.wikipedia.org/wiki/Huffman_coding) and using the english letter frequency
// (https://en.wikipedia.org/wiki/Letter_frequency#Relative_frequencies_of_letters_in_the_English_language).
// Some bits combinations are put aside for diverse reasons:
// - `00000`, to avoid confusion with a space,
// - `00001` and `10000` to force each letter to be connected horizontally, avoiding to generate confusing glyphs with
//   two distinct parts (`00001` followed by `10000` will not be connected because the longer link is 2x1).

/* eslint-disable quote-props */
const alphabet = {
  'e': 0b00010,
  't': 0b00100,
  'a': 0b01000,
  'o': 0b00011,
  'i': 0b00110,
  'n': 0b01100,
  's': 0b11000,
  'h': 0b00101,
  'r': 0b01010,
  'd': 0b10100,
  'l': 0b01001,
  'c': 0b10010,
  'u': 0b10001,
  'm': 0b00111,
  'w': 0b01011,
  'f': 0b01101,
  'g': 0b01110,
  'y': 0b10110,
  'p': 0b11010,
  'b': 0b11100,
  'v': 0b10011,
  'k': 0b10101,
  'j': 0b11001,
  'x': 0b11011,
  'q': 0b10111,
  'z': 0b11101,

  'E': 0b00010,
  'T': 0b00100,
  'A': 0b01000,
  'O': 0b00011,
  'I': 0b00110,
  'N': 0b01100,
  'S': 0b11000,
  'H': 0b00101,
  'R': 0b01010,
  'D': 0b10100,
  'L': 0b01001,
  'C': 0b10010,
  'U': 0b10001,
  'M': 0b00111,
  'W': 0b01011,
  'F': 0b01101,
  'G': 0b01110,
  'Y': 0b10110,
  'P': 0b11010,
  'B': 0b11100,
  'V': 0b10011,
  'K': 0b10101,
  'J': 0b11001,
  'X': 0b11011,
  'Q': 0b10111,
  'Z': 0b11101,

  // Unused bits combinations:
  // - 0b00001
  // - 0b10000
  // - 0b00000
  // - 0b01111
  // - 0b11110
  // - 0b11111
};
/* eslint-enable quote-props */

export default alphabet;
