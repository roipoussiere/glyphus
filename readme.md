# Glyphus

Procedurally generated glyphs

## What is it?

Glyphus is an alphabet created with the aim of being both very optimized and
beautiful.

Each glyph represents one word, and each word is represented by one glyph.
But all of them are generated in such a way that you can learn how to read it
with some training, based on simple rules, without having to remember each
glyph signification.

### Why?

> *Cyrano de Bergerac* : Que dites-vous ?… C’est inutile ?… Je le sais !
> 
> Mais on ne se bat pas dans l’espoir du succès !
>
> Non ! non, c’est bien plus beau lorsque c’est inutile !

*Cyrano de Bergerac, Edmond Rostand, acte V, scène 6*

### Is it a typeface?

Not really. [According to Wikipedia](https://en.wikipedia.org/wiki/Typeface), a
typeface is *a collection of glyphs, each of which represents an individual
letter, number, punctuation mark, or other symbol.*. Here a glyph represents a
full word. Furthermore, Glyphus can not be stored in a TrueType font format
(ie. `.ttf`) for technical reasons and works in the browser with Javascript.

## Trying Glyphus now

Just go to [the online demo page](http://roipoussiere.frama.io/glyphus)!

## Running Glyphus website locally

1. Get the repository:

- **option a**: by cloning it: `git clone https://framagit.org/roipoussiere/glyphus.git`
- **option b**: by downloading the archive: download
[the zip archive](https://framagit.org/roipoussiere/glyphus/-/archive/master/glyphus-master.zip)
(or chose other archive formats from the download button on the repository
page), then extract this archive.

2. Open `website/index.html` with you favorite web browser.

## Contributing

Thank you for you interest! ❤️ You can contribute to Glyphus in several ways.

### Talk around you

If you like the project, please share it to people, whether it is on you
favorite social media or away from keyboard.

### Discuss or provide feedback

If you are a typography enthusiast, type designer, speech therapist, or you
think that we can have interesting talks in some way, I definitely want to
hear you!

You can either contact me directly (see contact section below) or
[create an issue on GitLab](https://framagit.org/roipoussiere/glyphus/issues).

### Send pull requests

Please discuss with me before starting a pull request. ;)

For the rest, as usual, fork the repository, create a new branch, do you modifications,
then create a pull request on GitLab.

Don't forget to test your code before sending a pull request (see code section below).

## Code

Here we go.

You can start some hackings project after downloaded it (see *Running Glyphus
website locally* above) without other setups.

### Dependencies

[npm](https://www.npmjs.com/) is required to execute the build scripts used to
test code, provides a general-purpose package and publish the demo website.

### Create a package and publish

These steps are executed by the continuous integration to publish the website:

```
npm install
npm run build
```

This will package glyphus as a library in `dist`, then create a web folder in
`public` used in production.

### Test

Test your code with `npm test`? This which will run
[eslint](https://eslint.org/) according to the rules defined in `.eslintrc`.

Then open `public/index.html` with your favorite web browser to check that
everything works properly. Note the `public` instead of `website`, which use
packaged version of Glyphus that aims to be published on npm.

In order to avoid mistakes, you can create a git hook that executes `npm test`
before each commit (also works before each push with `pre-push`):

```
echo 'npm run build' > .git/hooks/pre-commit
chmod u+x .git/hooks/pre-commit
```

### Publish

You may be interested my the `npm start` command, that starts a web server and
serve the Glyphus website at `http://127.0.0.1:8080/`. This is required to test
the web page on some browsers like *Chromium*, which blocks the glyphus import.

If port `8080` is already used, you can define an other with
`npm config set glyphus:port <port>`.

## Contact

- `roipoussiere` *at* `protonmail` *dot* `com`;
- [Mastodon](https://mastodon.tetaneutral.net/@roipoussiere);
- [Twitter](https://twitter.com/roipoussiere);

## Licence

[MIT](./LICENCE)
